== README

Sample app to see how to make Angular.js Coffeescript and Rails work together.

This is a todo list app.

== Howto
First prepare the app with "bundle exec rake db:migrate" then, run the demo with "rails s".

Then register a new user (see the part on mail servers), then you can go to the lists and create a new one. Creating new elements in lists is not working currently and there is no design yet.

You need to run a mail server, like [mailcatcher](http://mailcatcher.me/), in order to confirm your registration.
