json.extract! @list, :id, :title, :updated_at, :user_id
json.array!(@list_elements) do |element|
	json.extract! element, :id, :content, :done
end