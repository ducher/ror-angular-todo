json.array!(@lists) do |list|
  json.extract! list, :id, :title, :updated_at, :user_id, :list_elements
end
