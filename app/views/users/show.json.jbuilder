json.extract! @user, :id, :email, :created_at, :updated_at
json.lists @lists do |list|
  json.id    list.id
  json.title list.title
end