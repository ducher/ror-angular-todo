# This line is related to our Angular app, not to our
# HomeCtrl specifically. This is basically how we tell
# Angular about the existence of our application.
@todoapp = angular.module 'todoapp', ['ngRoute', 'ng-token-auth', 'noteModule', 'listModule']

# This routing directive tells Angular about the default
# route for our application. The term "otherwise" here
# might seem somewhat awkward, but it will make more
# sense as we add more routes to our application.
@todoapp.config [
  '$routeProvider',
  '$authProvider',
  ($routeProvider, $authProvider) ->
    $routeProvider
    .when '/notes',
        templateUrl: 'assets/notes.html',
        controller: 'noteController'

    .when '/lists',
        templateUrl: 'assets/lists.html',
        controller: 'listController'

    .otherwise
      templateUrl: 'assets/home.html',
      controller: 'HomeCtrl'

    $authProvider
    .configure({
      apiUrl:                  window.location.href,
      tokenValidationPath:     '/auth/validate_token',
      signOutUrl:              '/auth/sign_out',
      emailRegistrationPath:   '/auth',
      accountUpdatePath:       '/auth',
      accountDeletePath:       '/auth',
      confirmationSuccessUrl:  window.location.href,
      passwordResetPath:       '/auth/password'
      passwordUpdatePath:      '/auth/password'
      passwordResetSuccessUrl: window.location.href
      emailSignInPath:         '/auth/sign_in',
      storage:                 'cookies',
      proxyIf:                 ()-> false,
      proxyUrl:                '/proxy',
      tokenFormat: {
        "access-token": "{{ token }}",
        "token-type":   "Bearer",
        "client":       "{{ clientId }}",
        "expiry":       "{{ expiry }}",
        "uid":          "{{ uid }}"
      },
      parseExpiry: (headers)->
        # convert from UTC ruby (seconds) to UTC js (milliseconds)
        parseInt headers['expiry'] * 1000 || null
      ,
      handleLoginResponse: (response)->
        response.data
      ,
      handleAccountResponse: (response)->
        response.data
      ,
      handleTokenValidationResponse: (response)->
        response.data
      
    })
]