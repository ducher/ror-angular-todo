

ListController = ($scope, listService)-> 
  $scope.list = "Todo List"
  $scope.lists = listService.query()

  $scope.addList = ->
  	newList = new listService({title:$scope.listTitle})
  	#$scope.lists.push({title:$scope.listTitle, id:4})
  	newList.$save()
  	$scope.lists.push(newList)
		$scope.listTitle = ''

ListService = ($resource, $auth)->
	#$resource($auth.apiUrl() +'/users/'+ '1' +'/lists.json')
	$resource($auth.apiUrl()+'/lists.json')
	###
	 return $resource('velos/:veloId', {}, {
    query: {method:'GET', params:{phoneId:''}, isArray:true}
  });
  ###

listModule = angular.module('listModule', ['ngResource'])

# Services 

listModule.factory('listService', ['$resource', '$auth', ListService])

# Controllers 

listModule.controller('listController', ['$scope', 'listService', ListController] )