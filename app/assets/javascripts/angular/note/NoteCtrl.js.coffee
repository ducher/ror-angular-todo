

NoteController = ($scope, noteService)-> 
  $scope.note = "PostIts"
  $scope.notes = noteService.query()

NoteService = ($resource, $auth)->
	$resource($auth.apiUrl() + '/notes.json')
	###
	 return $resource('velos/:veloId', {}, {
    query: {method:'GET', params:{phoneId:''}, isArray:true}
  });
  ###

noteModule = angular.module('noteModule', ['ngResource'])

# Services 

noteModule.factory('noteService', ['$resource', '$auth', NoteService])

# Controllers 

noteModule.controller('noteController', ['$scope', 'noteService', NoteController] )