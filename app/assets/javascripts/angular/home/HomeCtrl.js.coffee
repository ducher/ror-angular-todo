@todoapp.controller 'HomeCtrl', ['$scope','$auth','$log', ($scope,$auth,$log) ->
  $scope.handleRegBtnClick = () ->
    $auth.submitRegistration($scope.registrationForm)
      .then (resp) ->
        # handle success response
        $log.log "Can register :)"
        $log.error resp
      
      .catch (resp) -> 
        # handle error response
        $log.log "Can't register :("
        $log.error resp

  $scope.handleLoginBtnClick = () -> 
      console.log("logging in")
      $auth.submitLogin($scope.loginForm)
        .then (resp)->
          # handle success response
          console.log('logged in!')
          alert 'logged in'
        
        .catch (resp)->
          # handle error response
          console.log('can\'t log in !')
          alert 'not logged in'
        
  $scope.$on 'auth:login-success', (ev, user)->
    alert('Welcome ', user.email)

  $scope.$on 'auth:login-error', (ev, reason)->
    alert('auth failed because', reason.errors[0])

]