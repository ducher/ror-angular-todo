class User < ActiveRecord::Base
	has_many :lists, dependent: :destroy
  include DeviseTokenAuth::Concerns::User
end
