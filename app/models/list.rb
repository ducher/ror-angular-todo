class List < ActiveRecord::Base
  belongs_to :user
  has_many :list_elements, dependent: :destroy
  default_scope -> { order('created_at DESC') }
  validates :user_id, presence: true
end
