class ListsController < ApplicationController
  before_action :set_list, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    # @lists = List.all

    # if params[:user_id]
    #   @user = User.where(:id => params[:user_id]).first
    #   @lists = @user.lists
    # end
    @user = current_user
    @lists = @user.lists
  end

  def create
    @user = current_user
    @list = @user.lists.build(title: params[:title])

    respond_to do |format|
      if @list.save
        format.json { render :show, status: :created, location: @list }
      else
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_list
      @list = List.find(params[:id])
      @list_elements = @list.list_elements
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def list_params
      params.require(:list).permit(:content)
  	end
end
