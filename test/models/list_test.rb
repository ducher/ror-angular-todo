require 'test_helper'

class ListTest < ActiveSupport::TestCase

  def setup
    @user = User.create(email: 'lol@lol.lol')
    # This code is not idiomatically correct.
    @list = @user.lists.build(title: "Lorem ipsum")
  end

  test "list should be valid" do
    assert @list.valid?
  end

  test "user id should be present" do
    @list.user_id = nil
    assert_not @list.valid?
  end
end