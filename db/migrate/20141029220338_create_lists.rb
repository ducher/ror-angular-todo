class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.text :title
      t.references :user, index: true

      t.timestamps
    end
  end
end
