class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.string :content

      t.timestamps
    end
    add_index :microposts, [:user_id, :created_at]
  end
end
