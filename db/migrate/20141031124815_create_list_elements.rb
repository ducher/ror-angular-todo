class CreateListElements < ActiveRecord::Migration
  def change
    create_table :list_elements do |t|
      t.text :content
      t.boolean :done
      t.references :list, index: true

      t.timestamps
    end
  end
end
