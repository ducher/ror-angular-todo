# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

lists = List.create([{user_id: 1, title: "New list"}])
lists = List.create([{user_id: 2, title: "Groceries"}])
list_elements = ListElement.create([{content: "Butter", done:false, list_id: 2},
	{content:"Smooth", done:true, list_id:2}])